class AddSetting < ActiveRecord::Migration
  def change
  	create_table :settings do |t|
      t.text 	  :url
      t.string	:email
      t.string	:password
      t.text	  :token

      t.timestamps
    end
  end
end
