class UsersController < ApplicationController  
  before_filter :authenticate_user!

  def get_resourcemap_token
    resourcemap = Resourcemap.new(params[:url], params[:email], params[:password], "")
    token = resourcemap.connect()
    render json: token if token
    head :forbidden unless token
  end

  
end