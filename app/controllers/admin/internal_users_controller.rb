class Admin::InternalUsersController < ApplicationController
	before_filter :authenticate_user!
  before_filter :validate_user

  def index
    @user = User.where('type_id != ? and type_id != ?',1, 2)
    render layout: "admin"
  end

  def my_profile
    current_user.profile.dob = current_user.profile.dob.strftime("%m-%d-%Y") if current_user.profile.dob
    render layout: "admin"
  end

  def update
    @profile = current_user.profile
    if(@profile.update_attributes(params[:profile]))
      @profile.dob = DateTime.strptime(params["profile"]["dob"], "%m-%d-%Y")  unless params["profile"]["dob"].strip.empty?
      @profile.save
      flash[:notice] = "You have successfully updated your profile."
      redirect_to admin_projects_path()
    else
      render :my_profile
    end
    render layout: "admin"
  end

  def change_password
    @user = current_user
    render layout: "admin"

  end

  def update_password
    @user = current_user
    if(params["user"]["password"] != "" and params["user"]["password_confirmation"] != "")
      if(params["user"]["password"] == params["user"]["password_confirmation"])
        if @user.valid_password? params["user"]["current_password"]
          if(@user.update_attributes!(params[:user]))
            flash[:notice] = "You have successfully updated user #{@user.first_name} #{@user.last_name}."
            redirect_to admin_internal_users_path
          else
            flash[:error] = "Failed to update user password. Please try again later."
            render :change_password
          end
        else
          @user.errors.add(:current_password, " does not match" )
          render :change_password
        end
      else
        @user.errors.add(:password," and Password confirmation is not match")
        render :change_password
      end
    else
      @user.errors.add(:password," and Password confirmation can not be empty")
      render :change_password
    end
  end

  private
  def validate_user
    head :forbidden unless current_user.type.name == 'admin'
  end

end