class Admin::FreelancersController < ApplicationController
	before_filter :authenticate_user!
  before_filter :validate_user

  def index
    @user = User.where('type_id = ?', 1)
    render layout: 'admin'
  end

  def edit
    @profile = User.find_by_id(params[:id]).profile
    unless @profile.dob
      @profile.dob = DateTime.now().strftime("%d-%m-%Y")
    else
      @profile.dob = @profile.dob.strftime("%d-%m-%Y")
    end
    render layout: 'admin'
  end

  def update
    @profile = User.find_by_id(params[:id]).profile
    if(@profile.update_attributes(params[:profile]))
      @profile.dob = DateTime.strptime(params["profile"]["dob"], "%d-%m-%Y")  unless params["profile"]["dob"].strip.empty?
      @profile.save
      flash[:notice] = "You have successfully updated your profile."
      redirect_to admin_freelancer_path(@profile.user)
    else
      render :edit
    end
  end

  def destroy
     @user = User.find_by_id(params[:id])
    if @user.destroy!
      redirect_to admin_freelancers_path()
    else
      flash[:error] = "Failed to delete user."
      redirect_to admin_freelancers_path()
    end
  end

  def show
    @profile = User.find_by_id(params[:id]).profile
    @profile.dob = @profile.dob.strftime("%d-%m-%Y") if @profile.dob
    render layout: 'admin'
  end

  private
  def validate_user
    head :forbidden unless current_user.type.name == 'admin'
  end

end