class PagesController < ApplicationController

  PER_PAGE = 20  
  
	def index
    if current_user && !params[:explicit]
      redirect_to welcome_path 
    else
      redirect_to new_user_session_path 
    end
  end

  def show_collection
    setting = Setting.first()
    resourcemap = Resourcemap.new(setting.url, setting.email, setting.password, setting.token)
    @collection = resourcemap.get_collection(params[:id])
  end

  def welcome
    setting = Setting.first()
    resourcemap = Resourcemap.new(setting.url, setting.email, setting.password, setting.token)
    page  = params[:collection_page]? params[:collection_page]:1
    variable = {}
    variable["from"] = params[:from]
    variable["to"] = params[:to]
    variable["name"] = params[:name]
    variable["user"] = params[:email]
    variable["limit"] = PER_PAGE
    variable["offset"] =  PER_PAGE * (page.to_i - 1)
    variable["sortColumn"] = params[:sortColumn]
    variable["sortType"] = params[:sortType]
    collections = resourcemap.request_collection(variable)
    @collections = collections["collections"]
    @total = collections["total"]
    @paging = ([1] * @total.to_i).paginate(:page => page, :per_page => PER_PAGE)
  end
end