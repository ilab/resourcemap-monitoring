// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap
//= require turbolinks
//= require_tree .


function connectToResourcemap(){
  var _this = this;
  url = $("#setting_url").val();
  password = $("#setting_password").val();
  email = $("#setting_email").val();
  jQuery.ajax({
    url: "/get_authtoken",
    type: "POST",
    data: {"url" : url, "email": email, "password": password},
    dataType: 'json',
    complete: function(response) {
      if(response["status"] == 200){
        $("#setting_token").val(response["responseText"]);
      }
      else{
        $("#setting_token").val("");
        alert("Can't connect to resourcemap application")
      }
    }
  })
}

function submitData(){
  name = $("#name").val();
  email = $("#email").val();
  from = $("#from").val();
  to = $("#to").val();
  column = $("#paramSortColumn").val();
  type = $("#paramSortType").val();
  window.location.href = encodeURI("/welcome?name="+name+"&email="+email+"&from="+from+"&to="+to+"&sortColumn="+column+"&sortType="+type)
}

function sortHeader(column, type){
  name = $("#paramName").val();
  email = $("#paramEmail").val();
  from = $("#paramFrom").val();
  to = $("#paramTo").val();
  window.location.href = encodeURI("/welcome?name="+name+"&email="+email+"&from="+from+"&to="+to+"&sortColumn="+column+"&sortType="+type)
}




