class Resourcemap
	def initialize(url, email, password, token)
      @url = url
      @email = email
      @password = password
      @token = token
  end

  def connect()
  	url = @url + "/api/users/sign_in"
    variable = {'user[email]' => @email, 'user[password]' => @password}
    res = request_resourcemap(url, variable, "post")
    if res.code == 201
      return JSON.parse(res.response_body)["auth_token"]
    else
      return nil
    end
  end

  def request_collection params
  	url = @url + "/api/v1/admin/collections.json"
    params['auth_token'] = @token
    res = request_resourcemap(url, params, "get")
    if res.code == 200
      collections = JSON.parse(res.response_body)
      return collections
    else
      return nil
    end
  end


  def get_collection id
    url = @url + "/api/v1/admin/collections/#{id}.json"
    res = request_resourcemap(url, {'auth_token' => @token}, "get")
    if res.code == 200
      collection = JSON.parse(res.response_body)
      return collection
    else
      return nil
    end
  end

  def request_resourcemap(url, variable, method)
    request = Typhoeus::Request.new(
      url,
      method: method.to_sym,
      body: "this is a request body",
      params: variable,
      headers: { Accept: "text/html" }
    )
    request.run
    response = request.response
    return response
  end
end