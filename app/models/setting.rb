class Setting < ActiveRecord::Base    
  validates_presence_of :url

  attr_accessible :url
  attr_accessible :email
  attr_accessible :password
  attr_accessible :token

end